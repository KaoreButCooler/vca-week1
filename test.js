//first NodeJS application
const express = require('express') 
// Link the app to the express framework
const app = express() 
// Create an instance of an express RESTful service

// A 'RESTful' service is a service built on the REST architecture
// REST Architecture - A software architectural style that defines a set of rules to be used for creating web services.
// REST uses HTTP requests to access and use data.

const port = 3000 
// Define a port for the application service to listen on.
app.get('/', (req, res) => {
 res.send('Hello Virtual Machine!')
})
// Creates a server hook - a script that automatically runs every time an event occurs,
// triggered by a GET request to '/', the root directory. 
// A method is also passed to the GET function to be called when
// the resource is accessed. Inside this function, the
// res object is accessed to send the text 'Hello Virtual Machine!'

app.listen(port, () => {
 console.log(`Express Application listening at port 3000`)
})
// The listen method is used to bind to the port we previously defined,
// and the second argument passed is a method that calls console.log to 
// print an output to the terminal.
